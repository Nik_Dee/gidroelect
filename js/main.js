
// "use strict";

// показывает поле поиска
function searchToogle(el) {
	var form = $('#search'),
	input = form.find('input');

	if (input.css('left') === '0px') {
		input.css('left','100%');
	}


	$(el).toggleClass( "close_search" );

	form.toggleClass( "visible" );

	input.toggleClass( "visible" );

	input.animate({left: "0"}, 500, function () {
		
	});

	
}

// модальное окно
function openModal(ev) {
	var modal = $('#chechRing'),
	target = $(ev.target);


	console.log(ev.target);
	if (target.hasClass('button') || target.hasClass('block_wrap')) {
		modal.toggleClass( "visible" );
	}

	
}

// custom select
function customSelect(el) {
	var select = $(el),
	options = select.find('#allOptions');

	options.toggleClass( "visible" );
}

// выбирает активный option для select
function checkOption(el) {
	var options = $(el).closest('#allOptions'),
	options = options.find('li'),
	current = options.closest('#customSelect').find('#currentValue');

	current.text($(el).text());
	options.toggleClass( "visible" );
}

// скрывает текстовый блок в слайдере
function triggerShowContent(el) {
	var content = $(el).closest(".overlay").find('#content'),
	overlay = content.closest('.overlay'),
	time,folded;

	if (overlay.hasClass("folded")) {
		time = 0;
		folded = true;
	}else {
		time = 500;
		folded = false;
	}

	$(content).slideToggle( time, function () {
		overlay.toggleClass( "folded" );
		if (folded) {
			$(el).text('Свернуть');
		}else {
			$(el).text('Развернуть');
		}
	});

	
}

$(document).ready(function() {
	

	(function firstLetterCircle() {
		var title = $('.title_wrap ');

		title.each(function(index, el) {
			$(el).find('.first').text('.');
			$(el).find('.last').text('.');
		});
			
		})();

	

	// маска для ввода номера телефона
	$("#phone").mask("+3 (099)999-99-99");

	$('#mainSlider').slick({
		arrows: false,
		dots: true,
	  	autoplay: true,
	  	autoplaySpeed: 10000,
	  	customPaging: function(slider, i) {
	  	     return '<button></button>';
	  	   },
	});

	$('#projectsSlider').slick({
		arrows: false,
		dots: true,
	  	// autoplay: true,
	  	// autoplaySpeed: 10000,
	  	customPaging: function(slider, i) {
	  	     return '<button></button>';
	  	   },
	});

	// анимация при скроле 
	$('#projectsBlock').viewportChecker({
	    offset: 50+'%',
	    repeat: false,
	    callbackFunction: function(elem, action) {
	    	var top = $(elem).find('.top_line');
	    	var right = $(elem).find('.right_line');
	    	var bottom = $(elem).find('.bottom_line');  

		    top.animate({width: "95%"}, 1500,
		    	function() {
		    		right.animate({height: "95%"
		    	}, 1500, function() {
		    				bottom.animate({width: "550px"
			    		}, 1500, function() {
		    				bottom.find('.circle').show();
				    	});
			    	});
		    });
	    }
	});

	$('#footerBlock').viewportChecker({
	    offset: 10+'%',
	    repeat: false,
	    callbackFunction: function(elem, action) {
	    	var top = $(elem).find('.top_line');
	    	var right = $(elem).find('.right_line');
	    	var left = $(elem).find('.left_line');  
	    	var rightBottom = $(elem).find('.right_bottom_line');
	    	var leftBottom = $(elem).find('.left_bottom_line');  

		    top.animate({width: "100%"}, 1500,
		    	function() {
		    		right.animate({height: "100%"}, 1500,
		    			function() {
		    				rightBottom.animate({width: "35%"}, 1500,
		    					function(){
		    						rightBottom.find('.circle').show();
		    					});
		    			});
		    		left.animate({height: "100%"}, 1500,
		    			function() {
		    				leftBottom.animate({width: "35%"}, 1500,
		    					function(){
		    						leftBottom.find('.circle').show();
		    					});
		    			});
		    });
	    }
	});


	// doneProjectPage page
	if ($('#doneProjectPage').length > 0) {

		$('#articleBlock').viewportChecker({
		    offset: 10+'%',
		    repeat: false,
		    callbackFunction: function(elem, action) {
		    	var top = $(elem).find('.top_line'),
		    	left = $(elem).find('.left_line');  

			    top.animate({width: "462px"}, 1500,
			    	function() {
			    		left.animate({height: "478px"}, 1500,
			    			function() {
			    				left.find('.circle').show();
			    			});
			    	});
		    }
		});
	}else if ($('#forFactoryPage').length > 0) {

		$('#articleBlock').viewportChecker({
		    offset: 10+'%',
		    repeat: false,
		    callbackFunction: function(elem, action) {
		    	var top = $(elem).find('.top_line'),
		    	left = $(elem).find('.left_line');  

			    top.animate({width: "462px"}, 1500,
			    	function() {
			    		left.animate({height: "300px"}, 1500,
			    			function() {
			    				left.find('.circle').show();
			    			});
			    	});
		    }
		});
	}



	 function initialize() {
    	var lating = new google.maps.LatLng(.9721592919911,4656114621319);
    	var mapOptions = {
    		disableDefaultUI: true, // отключение интерфейса, чтобы отображалась только карта
    		zoom: 8, // величина приближения карты
    		center: lating, // центрирование на координаты, которые указывали мы.
    		mapTypeId: google.maps.MapTypeId.ROADMAP 
    	}
    	var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    }
	

	

});